
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>TAPASU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tamil Nadu and Pondichery Association of Urologists" />


<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />	
<link rel="stylesheet" href="css/slider.css">
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>


</head>
<body>
<!--start-home-->

	<?php include('header.php')?>
<!--//header-top-->
 <!-- //Line Slider -->

<div class="top_banner two">
			<div class="container">
			       <div class="sub-hd-inner">
						<h3 class="tittle">ABOUT <span>US</span></h3>
					</div>
			</div>
		</div>
        <!-- /Line Slider -->

 	<div class="main-textgrids">
				 		<div class="container">
				 			<div class="col-md-5 ab-pic">
				 					<img src="images/ab.jpg" alt=" " />

				 			</div>
				 			<div class="col-md-7 ab-text">
				 				<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Tamil Nadu and Pondicherry Association of Urologists (TAPASU) is an organization of urologists 
								of Tamil Nadu and Puducherry. It is the first state association of urologists in India. TAPASU provides
								education through fellowship and sharing of scientific knowledge and technical expertise. TAPASU’s goal 
								is to represent all urologists practicing in Tamil Nadu and Puducherry
								numbering more than 700 and thereby enhance the quality of urological care provided by its members to the
								people of this region.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							TAPASU aims to be a premier professional association
							in India and represent the interests of practicing urologists 
							and affiliated scientists of the region. It endeavours to form an 
							active liaison between the Association of Southern Urologists as well as
							the other neighbouring state associations of urologists to promote education 
							and training for mutual benefit. Promoting training for young urologists from India
							by awarding scholarships and recoginising research work, works of excellence in academics 
							and clinical urology by the urologists in Tamil Nadu and Puducherry  with awards. The Association conducts an annual
							conference and CMEs for helping urologists update their knowledge and improve the quality of care to the patients. 
							Asociation seeks to advocate the interest of the urologists of the region as well the interest of the patients needing urological
							care with policy makers and also to interact with other related professional associations such as the IMA for coordinated action
							in areas of mutual interest.

 </p>
								
				 			</div>
				 			<div class="clearfix"> </div>
				 			<!--<div class="statements">
					 			<div class="col-md-7 mission">
					 					<h4>Mission <span>Statement</span></h4>
										
					 					<p style="color:black">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
										
										<ul class="ab">
												<li><a href="single.html">Lorem ipsum dolor sit amet,consectetur adipiscing elit</a></li>
												<li><a href="single.html">Pellentesque eu erat lacus,consectetur adipiscing elit</a></li>
												<li><a href="single.html">Lorem ipsum dolor sit amet,consectetur adipiscing elit</a></li>
												<li><a href="single.html">Lorem ipsum dolor sit amet,consectetur adipiscing elit</a></li>
												<li><a href="single.html">Pellentesque eu erat lacus,consectetur adipiscing elit</a></li>
											</ul> 
					 			</div>
					 			<div class="col-md-5 facts">
					 					<img src="images/ab1.jpg" alt=" " />

					 					
					 			</div>
					 			<div class="clearfix"> </div>
					 		</div> -->
				 		</div>
				 </div>		

	<?php include('footer.php')?>
		<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
								<!--end-smooth-scrolling-->
		<a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/bootstrap.js"></script>

</body>
</html>