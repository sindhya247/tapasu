<?php

 session_start();


 include('db.php');

  include "session.php";
$username=$_SESSION['username'];

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>TAPASU | GALLERY</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <!-- Theme style -->
  
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">


  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<?php  include("header.php"); ?>
<body class="hold-transition skin-blue sidebar-mini">
<?php  include("menu.php"); ?>

<div class="wrapper">



 <div class="content-wrapper">
 

				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

  <caption></caption>
 
  <thead>
    <tr>
	   <th>Event Title</th>
      <th>Photos</th>
      <th width="180">Action</th>
    </tr>
  </thead>
  <?php
	$query=mysql_query("select * from gallery")or die(mysql_error());
	while($row=mysql_fetch_array($query)){
	$id=$row['Photo_ID'];
	$title=$row['title'];
	?>
  <tbody>
    <tr>
	  <td><?php echo $row['title']; ?> </td>   
      <td><img class="img-rounded" src="<?php echo $row['Photo']; ?>" width="200" height="100"></td>
         <td width = "180">
		 <a href="edit_photo.php?id=<?php  echo $id;?>"    class="btn btn-warning" ><i class="icon-trash pencil-large"></i>&nbsp; Edit</a>
	<a data-toggle="modal" href="modal_delete_photo.php?id=<?php  echo $id;?>" class="btn btn-danger">  <i class="icon-trash icon-large"></i>&nbsp;Delete</a>
		 </td>

    </tr>
  </tbody>
  <?php } ?>
</table>

    <div class="hero-unit-white1">
	  <a button class="btn btn-mini btn-block btn-success" type="button" href="modal_addphotos.php"><i class="fa fa-plus"></i> Add Photos</button></a>

	  </div>
 

</div>

<?php  include("footer.php"); ?>
<!-- ./wrapper -->

<!-- jQuery 3 -->
			<!--medicinal-->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
