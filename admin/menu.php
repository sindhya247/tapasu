  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>  <?php echo $username?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
     
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="">
          <a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
         </li>
		 
         <li><a href="memberupload.php"><i class="fa fa-book"></i> <span>Membership List</span></a></li>  
		 <li><a href="galleryupload.php"><i class="fa fa-book"></i> <span>Gallery</span></a></li>
		 <li><a href="eventupload.php"><i class="fa fa-book"></i> <span>Events & News</span></a></li>
    
       
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>