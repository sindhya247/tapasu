
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>TAPASU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tamil Nadu and Pondichery Association of Urologists" />



<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />	
<link rel="stylesheet" href="css/slider.css">
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--/script-->


<link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" />

<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>


</head>
<body>
<!--start-home-->

	<?php include('header.php')?>
<!--//header-top-->
 <!-- //Line Slider -->

<div class="top_banner two">
			<div class="container">
			       <div class="sub-hd-inner">
						<h3 class="tittle">THE <span>CONSTITUTION</span></h3>
					</div>
			</div>
		</div>
        <!-- /Line Slider -->

<div class="typography">
	 <div class="container">
			<div class="grid_3 grid_4 wow fadeInLeft animated" data-wow-delay=".5s">
		     <h3 class="bars" style="color:#e55752">TAMIL NADU & PONDUCHERRY ASSOCIATION OF UROLOGISTS<br>

</h3>
		     <div class="bs-example">
				<div class=" mb-60" >
 <h4 style="    margin: 15px 0 7px;">1.TITLE:</h4>
The name of the Association shall be “THE TAMIL NADU & PONDICHERRY ASSOCIATION

OF UROLOGISTS” (Short title to be ‘TAPASU’) herein after referred to as ‘The Association’.<br>

 <style>
ul {
    display: block;
    list-style-type: disc;
    margin-top: 1em;
    margin-bottom: 1 em;
    margin-left: 0;
    margin-right: 0;
    padding-left: 40px;
}
</style>

<h4 style="    margin: 15px 0 7px;">2.REGISTERED OFFICE:</h4>
The office of the Association shall be the office of the Secretary.<br>

<h4 style="    margin: 15px 0 7px;">3.OBJECTS:</h4>
<ul>
<li>To promote high standards in the practice of Urology.<br>
<li>To associate together in one corporate body all scientific personnel actively interested in the practice of Urology in the States of Tamil Nadu and Pondicherry.</li>
<li>To foster high and uniform standards in post graduate training in Urology.</li>
<li>To promote research in Urology or other disciplines for the purpose of improving the practice of Urology.</li>
<li>To promote, establish or support any institution, scientific association or research organisation which is directed towards raising the standards of practice or teaching or research in Urology in Tamil Nadu and Pondicherry.</li>
<li>To promote the publication of scientific literature pertaining to Urological practice or research.</li>
<li>To endeavour, to establish and maintain liaison with other associations or organized bodies, in India or abroad, whose objects are in keeping with those of the Association.</li>
<li>To carry out any other activities which are incidental or conducive to the furtherance of the objects of the Association.</li>
<li>To advice and defend our members in legal matters pertaining to the profession.</li>
 
</ul>
<h4 style="    margin: 15px 0 7px;">4.MEMBERSHIP:</h4>
There shall be the following category of members.<br>
<ul>
<li>Full Members: Members who are fully qualified Urologists practicing within the States of Tamil Nadu and Pondicherry.</li>
<li>Affiliate Members: Fully qualified Urologists practicing outside Tamil Nadu and Pondicherry are eligible to become affiliate members.</li>
<li>Associate Members: Postgraduate students of Urology and other qualified practitioners of Modern Medicine practicing Urology as full time/ part time and other faculty members interested in the field of Urology. Members without MCh and DNB qualification are eligible to become Associate members. After 3 years if the council is satisfied that they are doing fulltime Urology, they will become full members.  For this they have to make an application to the Secretary.</li>
</ul>


<h4 style="    margin: 15px 0 7px;">5.MODE OF ELECTION:</h4>
An application for membership must be sponsored by two full members of “TAPASU” and has to be endorsed by the Council.<br>

<h4 style="    margin: 15px 0 7px;">6.RIGHTS OF MEMBERS:</h4>
<ul>
<li>Only full members without arrears shave the right to attend General Body meeting, to vote and hold any office.</li>
<li>All classes of members shall have the right to attend any Scientific and participate in all academic activities.</li>
</ul>

<h4 style="    margin: 15px 0 7px;">7.CESSATION OF MEMBERSHIP:</h4>
<ul>
<li>Any member may resign from his membership at any time by serving a notice in writing to the Secretary. Such resignation shall not relieve the member from the responsibility to pay any dues to the Association prior to ceasing to be a member.</li>
<li>The council shall have the right to terminate the membership or alter the class of any individual member, even after election, if it is subsequently found that materials supplied in the application were incorrect.</li>
<li>The Council shall have the powers to terminate the membership of any member:</li>
<li>Who has been found guilty of unprofessional behavior or of working against the interest of the Association provided that member concerned is offered full opportunity to present his case to the council, and provided that in case of termination of membership, the latter is confirmed at the Annual General Meeting.</li>
<li>Who was defaulted in paying Association dues for over a period of 2 Calendar years.</li>
</ul>

<h4 style="    margin: 15px 0 7px;">8.SUBSRIPTION:</h4>
The subscription shall be:<br>
<ol>
<li>The subscription shall be Rs. 200/- for full members (Annual).</li>
<li>Full members life subscription Rs. 750/-</li>
<li>Affiliate members(Life)-Rs.750/-</li>
<li>Associate members (Annual)-Rs.100/-</li>
</ol>

<h4 style="    margin: 15px 0 7px;">9.FINANCIAL YEAR:</h4>
The financial year of the association shall be from 1St April to 31 March.</br>

<h4 style="    margin: 15px 0 7px;">10.BANK ACCOUNT:</h4>
The bank account shall be operated in the name of the Association by the Treasurer.
 All cheques shall be signed by the Treasurer of the Association, and either by the President or Secretary.<br>

  <h4 style="    margin: 15px 0 7px;"> 11.OFFICERS OF THE ASSOCIATION:</h4>
<ul>
<li>Officers of the Association shall be the President, Vice –President, Secretary and Treasurer.</li>
<li>The Officers shall be elected at the Annual General Meeting from a list of valid nominations from among full members.</li>
<li>PRESIDENT: The Vice President for the given year will automatically become the President for the next year. He will hold the office for one year. 
 An election for the post of the President will be held only if the Vice-President for the previous year does not wish to or is not available 
 to take over the office of the President.</li>
<li>Vice-President: The Vice-President shall be elected for 1 year. He will automatically become the President of the following year.</li>
<li>Secretary: Will be elected for two years.</li>
<li>Treasurer: Will be elected for two years.</li>
<li>The office bearers, the President, Vice-President shall hold office from the duty following the termination of Annual General Meeting at
 which he is installed to the termination of the next annual meeting.</li>
<li>The President shall not be eligible for Re-election.</li>
<li>The Secretary & Treasurer will be eligible for re-election for one more term of two years.</li>
</ul>
 <h4 style="    margin: 15px 0 7px;">12.DUTIES OF THE OFFICERS:</h4>
 <ul>
<li>The President shall preside over all meetings of council and the Annual General Meeting.</li>
<li>The Vice-President shall perform all duties of the President in the absence of the latter or at his request.
The Vice-President shall have such other powers and functions as may be assigned by the council.</li>

<li>The Secretary shall look after the day-to-day affairs of the Association under direction of the Council. He shall be responsible.
For summoning all meetings.
for preparing the agendas and keeping minutes of proceedings
He shall be in charge of all files, documents, books and other papers of the Association.
He shall be responsible for maintaining a register of members.
He shall be responsible for formulating the annual report to be presented to the council and the Annual General Meeting.</li>
<li>The Treasurer shall be responsible for receiving all
 subscriptions and other dues from the members, for paying all bills and for preparing an audited statement
 of accounts and balance sheet annually to the council and the annual general meeting. He shall keep true and accurate records 
 of all financial transactions of the Association. On request from the Secretary, he shall pay authorized bills of the Association.  
 He shall be responsible for notifying all members regarding their dues to the Association.</li>
 
</ul>
<h4 style="    margin: 15px 0 7px;">13.COUNCIL:</h4>
The council of the Association shall consist of:-<br>
<ul>
<li>The officers elected as per rule 11, immediate past president and 6 members elected among full members representing various regions.</li>
<li>No member of the Association shall be eligible for election as an office bearer unless he has completed one year as a member of the Association at the time of election.</li>
<li>The member representatives in the council shall be elected to hold office for two years.</li>
<li>Nomination for office bearers & council members shall be called for by the Secretary 6 weeks before the date of the Annual General Meeting.</li>
<li>Nominations for office bearers & council members shall be made by two members, along with a written consent by the candidate agreeing to the nomination.</li>
<li>All nominations for election should reach the Secretary at least four weeks before the date of the annual meeting.</li>
<li>All members of the council shall be elected at the Annual General Meeting.</li>
<li>For any casual vacancy in the council that may occur, the council shall be empowered to appointing a successor for office or a council member, as the case may be until the next Annual General Meeting.</li>
</ul>
<h4 style="    margin: 15px 0 7px;">14.DUTIES OF THE COUNCIL:</h4>
<ul>
<li>The council shall constitute the executive authority of the Association in all matters scientific, business and financial.</li>
<li>The council shall be responsible for arrangements for the business and scientific parts of the Annual General Meeting.</li>
<li>The council shall have the powers to decide to convene such other scientific meetings besides the annual one as it deems desirable.</li>
<li>The council shall have the power to invest money of the Association with due regard for all existing laws.</li>
<li>The council will meet before the Annual General Body meeting at least once in the year.</li>
</ul>
<h4 style="    margin: 15px 0 7px;">15.GENERAL MEETING:</h4>
The Annual General Meeting shall consist of two parts.  Business and Scientific to be held during September, 
October each year.  Notice for the meetings shall be sent by the secretary six weeks before the date of annual meeting.<br>

 

The agenda for the Annual General Business meeting shall be:-<br>
<ul>
<li>To receive the report of the council</li>
<li>To receive the report of the Treasurer and the audited statement of accounts and balance sheet.</li>
<li>To approve new members of the Association as recommended by the council.</li>
<li>To elect officers and members of the council.</li>
<li>To consider other business as the council may determine.</li>
<li>To consider any resolution or amendments to the rules submitted by members in accordance with rules.</li>
</ul>

<h4 style="    margin: 15px 0 7px;">16.Quorum </h4>at Annual General meeting shall be constituted by fifteen full members. 
If no quorum is formed the meeting is dissolved.  Another meeting will be called as per decision of council.<br>

<h4 style="    margin: 15px 0 7px;">17.Resolution:</h4>
 Any members desirous of moving a resolution at the Annual General Meeting shall notify the same in writing
 to the Secretary at least four weeks before the date of such meeting.<br>

<h4 style="    margin: 15px 0 7px;">18.DISSOLUTION:</h4>
Not less than three-fifths of the members of Association may determine that the association shall be dissolved, 
and there upon it shall be dissolved forthwith, or at the time then agreed upon, and all necessary steps shall be 
taken  for disposal and settlement of property of the Association an termination of the accounts after discharge of all 
the debts of liabilities, shall remain property or funds whatsoever they shall be given or transferred to an institution or 
institutions having similar objectives as the Association which shall be determined by the vote of not less than three fifth 
of the members of the group at the meeting at the time of dissolution.<br>

<h4 style="    margin: 15px 0 7px;">19.AMMENDMENTS OF RULES:</h4>
Notice of any proposal to repeal, amend or add to the less existing rules shall be forwarded to the
 Secretary in writing not less than four weeks before the date of the Annual General Meeting.  All the
 amendments to the rules suggested by the member or by the members or by the council shall be circulated 
 to the members by the Secretary at least 3 weeks prior to the meeting.  The proposal shall appear on the agenda of the meeting in the form of a 
 resolution duly proposed and seconded.  No proposal shall be considered as passed unless it is carried by a majority of at least three-fifth of the
 members present and voting.</br>

 

 

 
<h4 style="    margin: 15px 0 7px;">
Amendments to the TAPASU Constitution passed in AGM, 2005 at Courtallam.</h4>

 

Article 13: Council: Amendment:<br>
<ul>
<li>
The Officers elected as per rule 11, immediate Past President and 10 members elected from among full members representing various regions.
</li></ul><br>

Article 14: Duties of the Council: Amendment:<br>
<ul>
<li>
7 members of the council should attend to form the quorum for the Council meeting.</li></ul><br>

Article 15:  General Meeting: Amendment:<br>
<ul><li>
The annual General meeting shall consist of two parts: Business and Scientific : – to be held during the last 4 months of the year.</li>
</ul><br>

</div>
			 </div>
	      </div>
		  
		  </div>
		  </div>
	
		
		
			<!--medicinal-->
	<?php include('footer.php')?>
		<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
								<!--end-smooth-scrolling-->
		<a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/bootstrap.js"></script>

</body>
</html>