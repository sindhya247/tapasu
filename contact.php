
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>TAPASU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tamil Nadu and Pondichery Association of Urologists" />


<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/slider.css">
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>
</head>
<body>
<!--start-home-->
	<?php include('header.php')?>
<!--//header-top-->
 <!-- //Line Slider -->
		<div class="top_banner two">
			<div class="container">
			       <div class="sub-hd-inner">
						<h3 class="tittle">CONTACT <span>US</span></h3>
					</div>
			</div>
		</div>
	<!--/contact-->
		<div class="contact-top">
		    <div class="container">


									<div class="col-md-4  modal-align">
										<div class="">
										<h6 style="    font-size: 20px;
    color: #e45753;
    font-weight: bold;" >ADDRESS</h6><br>
	<div><p style="    line-height: 24px;">
            Dr.T.R.Ghurunaath<br>

            18/13, Chairman Chidambaram Street,<br>
            West Shanmugapuram,</br>
            Villupuram - 605602,</br> </p><br>
					<h6 style="    font-size: 20px;
    color: #e45753;
    font-weight: bold;" >Email</h6><br>
					<p><a href="mailto:info.tapasu@gmail.com">info.tapasu@gmail.com</a></p><br>
					<h6 style="    font-size: 20px;
    color: #e45753;
    font-weight: bold;" >Phone Number</h6><br>
					<p> +91 9894621591</p><br>
										</div>
									</div>

									<div class="clearfix"></div>
								</div>
								<div  height=" 300px">
				  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.299819557789!2d80.27019091482316!3d13.080175190784736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5268a9d5786687%3A0x224a3cb04685fd47!2sMadras+Medical+College!5e0!3m2!1sen!2sin!4v1501760705697" width="50%" height="    height: 1000px;" frameborder="0" style="border:0" allowfullscreen></iframe>
				 </div>
				<div class="contact-form" style="    margin-top: 12em;">
			<div class="sub-hd">
						<h3 class="tittle two" style="color:#18184c">Reach out to<span> us</span></h3>
					</div>






			<div class="col-md-7 ">
					     <form method="post" action="mail.php" class="left_form">
					    	<div>
						    	<span><label>NAME</label></span>
						    	<span><input name="name" type="text" id="name"class="textbox"></span>
						    </div>
						    <div>
						    	<span><label>E-MAIL</label></span>
						    	<span><input name="email" type="text" id="email" class="textbox"></span>
						    </div>
						    <div>
						     	<span><label>Phone Number</label></span>
						    	<span><input name="phone" type="text" id="phone"class="textbox"></span>
						    </div>
					    </div>
							<div class="col-md-5 ">
								<div >
									<span><label>SUBJECT</label></span>
									<span><textarea name="msg"id="msg"> </textarea></span>
								</div>
							   <div>
									<span><input type="submit" value="send" name="submit" class="myButton"></span>
							  </div>	</div>
					    </form>
					    <div class="clearfix"></div>
				  </div>
			</div>
		</div>
	<!--//contact-->

	  <!--/start-footer-section-->
	<?php include('footer.php')?>
		<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear'
								 		};
										*/

										$().UItoTop({ easingType: 'easeOutQuart' });

									});
								</script>
								<!--end-smooth-scrolling-->
		<a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/bootstrap.js"></script>

</body>
</html>
