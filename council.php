
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>TAPASU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tamil Nadu and Pondichery Association of Urologists" />



<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/slider.css">
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--/script-->


<link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="css/font-awesome.min.css" />

<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>


</head>
<body>
<!--start-home-->

	<?php include('header.php')?>
<!--//header-top-->
 <!-- //Line Slider -->

<div class="top_banner two">
			<div class="container">
			       <div class="sub-hd-inner">
						<h3 class="tittle">OUR <span>TEAM</span></h3>
					</div>
			</div>
		</div>
        <!-- /Line Slider -->

 <section class="team text-center">
				<div class="container">
					<h3 class="title">OUR COUNCIL </h3>
					<p class="w-text"> </p>
					<div class="team-row">
						<div class="col-md-3 team-grids">
                            <div class="team-img">
                                <img src="images/MUTHU VEERAMANI.jpg" alt="">
                            </div>
							<h5><font style="color:white">Ramaswamy,</font><br>President</h5>
							<p>Dr Muthu Veeramani</p>

							<div class="abt-social-icons">


							</div>
							<div class="">

							</div>
						</div>
						<div class="col-md-3 team-grids team-mdl">
                            <div class="team-img">
                                <img src="images/dr-g-sivasankar-5a8677b4d6f11.jpg" alt="">
                            </div>
                            <h5>Vice President           </h5>
							<p>  Dr.Shiva Sankar</p>

							<div class="">

							</div>
						</div>
						<div class="col-md-3 team-grids team-mdl1">
                            <div class="team-img">
                                <img src="images/" alt="">
                            </div>
							<h5>Past President							</h5>
							<p>Dr.Devaprasath</p>

							<div class="">

							</div>
						</div>

							<div class="col-md-3 team-grids team-mdl1">
                                <div class="team-img">
                                    <img src="images/TR GHURUNAATH.jpeg" alt="">
                                </div>
							<h5><font style="color:white">Ramaswamy,</font><br>Secretary   </h5>
							<p>Dr.T.R.Ghurunaath</p>


							<!--
							<div class="team-img">
								<img src="images/d3.jpg" alt="">
							</div> -->
						</div>
							<div class="col-md-3 team-grids">
                                <div class="team-img">
                                    <img src="images/DR DHINAKAR BABU.jpeg" alt="">
                                </div>
							<h5><font style="color:white">Ramaswamy,</font><br>Treasurer                     </h5>
							<p>Dr.Dinakar Babu N</p>

							<div class="">

							</div>
						</div>
						<div class="clearfix"> </div>
					</div>
				</div>
			<!--//team-->
		</section>
			<div class="main-textgrids">
				<div class="container">
				    <div class="sub-hd">
						<h3 class="tittle two" style="color:#18184c">OUR <span>members</span></h3>
					</div>


						<div class="col-md-5 ab-pic">
				 					<img src="images/1.jpg" alt=" " />

				 			</div>
				 			<div class="col-md-7 ab-text">
				 				<ul class="ab">





                                    <li>Dr.Rajesh Rajendran</li>
                                    <li>Dr.Subramaniyan</li>
                                    <li>Dr.Kalyanram Kone</li>
                                    <li> Dr.Gowdhaman S</li>
                                    <li> Dr.Arun Kumar</li>
                                    <li> Dr.Ahmed Marzook</li>
                                    <li>Dr.Gopinath M</li>
                                    <li>Dr.Induja J</li>
                                    <li> Dr.Deepak David</li>
                                    <li> Dr.Subha Kanesh S K</li>
											</ul>



				 			</div>

				</div>
			</div>


			<!--medicinal-->
	<?php include('footer.php')?>
		<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear'
								 		};
										*/

										$().UItoTop({ easingType: 'easeOutQuart' });

									});
								</script>
								<!--end-smooth-scrolling-->
		<a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/bootstrap.js"></script>

</body>
</html>
