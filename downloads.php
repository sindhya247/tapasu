
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>TAPASU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tamil Nadu and Pondichery Association of Urologists" />


<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/slider.css">
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>


</head>
<body>
<!--start-home-->
	<?php include('header.php')?>
<!--//header-top-->
 <!-- //Line Slider -->
		<div class="top_banner two">
			<div class="container">
			       <div class="sub-hd-inner">
						<h3 class="tittle">Downloads<span></span></h3>
					</div>
			</div>
		</div>
			<!-- typography -->
<div class="typography">
	 <div class="container">

		  <div class="grid_3 grid_5 wow fadeInRight animated" data-wow-delay=".5s">
			<h3 class="bars">Abstract Submission Form for TAPASUCON 2022				<a href="attachment/abstract.docx"><span class="label label-success">DOWNLOAD IN WORD FORMAT</span></a></h3>
			<h3 class="bars">Abstract Submission Form for TAPASUCON 2022				<a href="attachment/abstract.pdf"><span class="label label-success">DOWNLOAD IN WORD PDF</span></a></h3>
			<h3 class="bars">	  Rules for TAPASU Awards and Travel Fellowships			<a href="attachment/Travel-Fellowships%20-%202023.pdf"><span class="label label-success">DOWNLOAD</span></a></h3>
		<h3 class="bars">	  Election Nomination Form		<a href="attachment/ELECTION-NOMINATION-FORM.pdf"><span class="label label-success">DOWNLOAD</span></a></h3>
		<h3 class="bars">	 Membership Form	<a href="attachment/newMembershipForm2023.pdf"><span class="label label-success">DOWNLOAD</span></a></h3>



		 </div>
		 </div>

<!-- //shortcodes -->

	<!--/start-footer-section-->
		<?php include('footer.php')?>
		<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear'
								 		};
										*/

										$().UItoTop({ easingType: 'easeOutQuart' });

									});
								</script>
								<!--end-smooth-scrolling-->
		<a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/bootstrap.js"></script>

</body>
</html>
