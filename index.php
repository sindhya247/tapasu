
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>TAPASU</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Tamil Nadu and Pondichery Association of Urologists" />

<link rel="stylesheet" href="css/font-awesome.min.css" />




<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/slider.css">
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>


</head>
<body>
<!--start-home-->

	<?php include('header.php')?>
<?php //include('popup.php')?><!-- -->

	 <div class="top_banner">
		<!-- SVG Arrows -->
		<div class="svg-wrap">
			<svg width="64" height="64" viewBox="0 0 64 64">
				<path id="arrow-left" d="M46.077 55.738c0.858 0.867 0.858 2.266 0 3.133s-2.243 0.867-3.101 0l-25.056-25.302c-0.858-0.867-0.858-2.269 0-3.133l25.056-25.306c0.858-0.867 2.243-0.867 3.101 0s0.858 2.266 0 3.133l-22.848 23.738 22.848 23.738z" />
			</svg>
			<svg width="64" height="64" viewBox="0 0 64 64">
				<path id="arrow-right" d="M17.919 55.738c-0.858 0.867-0.858 2.266 0 3.133s2.243 0.867 3.101 0l25.056-25.302c0.858-0.867 0.858-2.269 0-3.133l-25.056-25.306c-0.858-0.867-2.243-0.867-3.101 0s-0.858 2.266 0 3.133l22.848 23.738-22.848 23.738z" />
			</svg>
		</div>

		<div class="sleekslider">
			<!-- Slider Pages -->
			<div class="slide active bg-1">
				<div class="slide-container">


				</div>
			</div>
			<div class="slide bg-2">
				<div class="slide-container">

				</div>
			</div>
			<div class="slide bg-3">
				<div class="slide-container">

				</div>
			</div>
			<div class="slide bg-4">
				<div class="slide-container">

				</div>
			</div>
			<div class="slide bg-5">
				<div class="slide-container">

				</div>
			</div>

			<!-- Navigation Arrows with Thumbnails -->
			<nav class="nav-split">
				<a class="prev" href="">
					<span class="icon-wrap"><svg class="icon" width="22" height="22" viewBox="0 0 64 64"><use xlink:href="#arrow-left" /></svg></span>
					<div>
						<h3>test</h3>
						<img alt="Previous thumb"/>
					</div>
				</a>
				<a class="next" href="">
					<span class="icon-wrap"><svg class="icon" width="22" height="22" viewBox="0 0 64 64"><use xlink:href="#arrow-right" /></svg></span>
					<div>
						<h3>test</h3>
						<img alt="Next thumb"/>
					</div>
				</a>
			</nav>

			<!-- Pagination -->
			<nav class="pagination">
				<span class="current"></span>
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</nav>

			<!-- Navigation Tabs -->
			<!--<nav class="tabs">
				<div class="tab-container">
					<ul>
						<li class="current"><a href="#"><span>01</span> Slide</a></li>
						<li><a href="#"><span>02</span> Slide</a></li>
						<li><a href="#"><span>03</span> Slide</a></li>
						<li><a href="#"><span>04</span> Slide</a></li>
						<li><a href="#"><span>05</span> Slide</a></li>
					</ul>
				</div>
			</nav> -->
		</div>

		<!-- JavaScripts -->
		<script type="text/javascript" src="js/sleekslider.min.js"></script>
		<script type="text/javascript" src="js/app.js"></script>
      <!--welcome-->
 </div>

        <!-- /Line Slider -->
	</div>
    <!--footer-->

		<!-- Services -->
		<div class="services" id="services">
			<div class="container">



				<div class="inner_tabs">
				    <h2 class="tittle">Welcome to TAPASU</h2>
					<p class="sub-para">The Main Objective of TAPASU is to promote high standards in the practice of Urology.
and toassociate together in one corporate body all scientific personnel actively interested in the practice of Urology in the States of Tamil Nadu and Pondicherry.</p>
					<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
						<ul id="myTab" class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#expeditions" id="expeditions-tab" role="tab" data-toggle="tab" aria-controls="expeditions" aria-expanded="true">About Us</a></li>
							<!--<li role="presentation"><a href="#tours" role="tab" id="tours-tab" data-toggle="tab" aria-controls="tours">Register Now</a></li>
							<<li role="presentation"><a href="#tree" role="tab" id="tree-tab" data-toggle="tab" aria-controls="tree">Patient life</a></li>
							<li role="presentation"><a href="#safari" role="tab" id="safari-tab" data-toggle="tab" aria-controls="safari">Baby Birth</a></li>
							<li role="presentation"><a href="#trekking" role="tab" id="trekking-tab" data-toggle="tab" aria-controls="trekking">Lab Testing</a></li>
					-->	</ul>
						<div id="myTabContent" class="tab-content">
							<div role="tabpanel" class="tab-pane fade in active" id="expeditions" aria-labelledby="expeditions-tab">
								<div class="col-md-5 col-sm-5 tab-image">
									<img src="images/4.jpg" alt="Medicinal">
								</div>
								<div class="col-md-7 col-sm-7 tab-info">
									<p style="line-height: 1.5em;">  TAPASU aims to be a premier professional association in India and represent the interests of practicing urologists and affiliated scientists of the region. It endeavours to form an active liaison between the Association of Southern Urologists, UROLOGY SOCIETY OF INDIA ,and other neighbouring
                                        state associations of urologists to promote education and training for mutual benefit.  Promoting training for young urologists from India by awarding scholarships and recoginising research work,
									works of excellence in academics and clinical urology by the urologists in Tamil Nadu and Puducherry  with awards.
									<br>
									The Association conducts an annual
									conference and CMEs for helping urologists update their knowledge and improve the quality of care to the patients. Asociation seeks to advocate the
									interest of the urologists of the region as well the interest of the patients needing urological care with policy makers and
									also to interact with other related professional associations such as the IMA for coordinated action in areas of mutual interest.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tours" aria-labelledby="tours-tab">
								<div class="col-md-5 col-sm-5 tab-image">
									<img src="images/8.jpg" alt="Medicinal">
								</div>
								<div class="col-md-7 col-sm-7 tab-info">
									<p style="line-height: 1.5em;">
									The TAPASU  is composed of more than 500 members including urologists,
									residents and other professional and scientists in the field of urology and
									allied basic and clinical sciences predominantly from Tamil Nadu and Puducherry.
									TAPASU  provides a forum and networking for urologists of the region.<br><br>

Membership benefits includes:<br><br>

Scientific Meeting. An excellent scientific program is planned each year in the annual meeting of the Association August /September. Members
get a discounted registration for this conference. In addition a mid-term CUE is held in April / May every year.<br><br>

There are many awards and travel fellowships for members to encourage research and learning. During the annual conference of the society,
TAPASU presents awards for papers presented in various categories. There are awards for best papers, best video and best poster and an award for the the winner of a uro-radiology quiz. One senior member each year, who has made outstanding
contributions to the field of urology is recognised by awarding him/her the Dr. S.B. Umapathy Oration.<br></p><br>

Membership – Fee: Rs 4000 (Only Life membership fee ; No Annual Membership)<br><br>
									<h3 class="bars">	 Membership Form	<a href="attachment/Download-Membership-Form.pdf"><span class="label label-success">DOWNLOAD</span></a></h3>
								</div>
								<div class="clearfix"></div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="tree" aria-labelledby="tree-tab">
								<div class="col-md-5 col-sm-5 tab-image">
									<img src="images/5.jpg" alt="Medicinal">
								</div>
								<div class="col-md-7 col-sm-7 tab-info">
									<p> This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact form, accompanied by English versions from the 1914 translation by H. Rackham. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="safari" aria-labelledby="safari-tab">
								<div class="col-md-5 col-sm-5 tab-image">
									<img src="images/6.jpg" alt="Medicinal">
								</div>
								<div class="col-md-7 col-sm-7 tab-info">
									<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose.</p>
								</div>
								<div class="clearfix"></div>
							</div>
							<div role="tabpanel" class="tab-pane fade" id="trekking" aria-labelledby="trekking-tab">
								<div class="col-md-5 col-sm-5 tab-image">
									<img src="images/7.jpg" alt="Medicinal">
								</div>
								<div class="col-md-7 col-sm-7 tab-info">
									<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures.</p>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</div>
		<!-- //Services -->

			<!---news-->


	<!--/start-footer-section-->
	<?php include('footer.php')?>
		<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear'
								 		};
										*/

										$().UItoTop({ easingType: 'easeOutQuart' });

									});
								</script>
								<!--end-smooth-scrolling-->
		<a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/bootstrap.js"></script>

</body>
</html>
