<?php

	error_reporting( ~E_NOTICE ); // avoid notice
	
	require_once 'dbconfig.php';
	
	if(isset($_POST['btnsave']))
	{
		$username = $_POST['user_name'];// user name
		$dob = $_POST['dob'];// user email
			$age = $_POST['age'];	
			$sex = $_POST['sex'];
				$ppd = $_POST['ppd'];
					$mno = $_POST['mno'];
						$rno = $_POST['rno'];
							$padd = $_POST['padd'];
								$ppadd = $_POST['ppadd'];
									$qua = $_POST['qua'];
										$mem = $_POST['mem'];
									
		
		$imgFile = $_FILES['user_image']['name'];
		$tmp_dir = $_FILES['user_image']['tmp_name'];
		$imgSize = $_FILES['user_image']['size'];
		
		
		if(empty($username)){
			$errMSG = "Please Enter Username.";
		}
		
		else if(empty($imgFile)){
			$errMSG = "Please Select Image File.";
		}
		else
		{
			$upload_dir = 'admin/user_images/'; // upload directory
	
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
		
			// valid image extensions
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
		
			// rename uploading image
			$userpic = rand(1000,1000000).".".$imgExt;
				
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions)){			
				// Check file size '5MB'
				if($imgSize < 5000000)				{
					move_uploaded_file($tmp_dir,$upload_dir.$userpic);
				}
				else{
					$errMSG = "Sorry, your file is too large.";
				}
			}
			else{
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";		
			}
		}
		
		
		// if no error occured, continue ....
		if(!isset($errMSG))
		{
			$stmt = $DB_con->prepare('INSERT INTO tbl_users(userName,userPic,dob,age,sex,ppd,mno,rno,padd,ppadd,qua,mem) VALUES(:uname, :upic, :udob, :uage, :usex, :uppd, :umno, :urno, :upadd, :uppadd, :uqua, :umem)');
			$stmt->bindParam(':uname',$username);
			$stmt->bindParam(':upic',$userpic);
					$stmt->bindParam(':udob',$dob);
							$stmt->bindParam(':uage',$age);
									$stmt->bindParam(':usex',$sex);
											$stmt->bindParam(':uppd',$ppd);
													$stmt->bindParam(':umno',$mno);
															$stmt->bindParam(':urno',$rno);
																	$stmt->bindParam(':upadd',$padd);
																			$stmt->bindParam(':uppadd',$ppadd);
						$stmt->bindParam(':uqua',$qua);		
						$stmt->bindParam(':umem',$mem);
						
			if($stmt->execute())
			{
				$successMSG = "new record succesfully inserted ...";
				header("refresh:5;index.php"); // redirects image view page after 5 seconds.
			}
			else
			{
				$errMSG = "error while inserting....";
			}
		}
	}
?>
<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Medical</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Medicinal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
<script type="applisalonion/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />	
<link rel="stylesheet" href="css/slider.css">
<script src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<!--/web-font-->
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<!--/script-->
<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},900);
				});
			});
</script>


</head>
<body>
<!--start-home-->

	<?php include('header.php')?>
<!--//header-top-->
 <!-- //Line Slider -->


        <!-- /Line Slider -->

    <!--footer-->
	
		<!-- Services -->
		<div class="services" id="services">
			<div class="container">

				<div class="inner-w3">
					    <div class="sub-hd">
						<h3 class="tittle"><span>MEMBERSHIP APPLICATION FORM
</span></h3>
					</div>
					</div>
				 
				<div class="inner_tabs">
				    <h2 class="tittle">
</h2>
					To<br>
The Secretary<br>
TAPASU<br><br>
Sir,<br>
I hereby apply for enrolment as<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(a) Life<br>
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(b) Full/ Affiliate /Associate member of TAPASU. <br><br><br>
					<div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
					
						<div class="container">


    

	<?php
	if(isset($errMSG)){
			?>
            <div class="alert alert-danger">
            	<span class="glyphicon glyphicon-info-sign"></span> <strong><?php echo $errMSG; ?></strong>
            </div>
            <?php
	}
	else if(isset($successMSG)){
		?>
        <div class="alert alert-success">
              <strong><span class="glyphicon glyphicon-info-sign"></span> <?php echo $successMSG; ?></strong>
        </div>
        <?php
	}
	?>   

<form method="post" enctype="multipart/form-data" class="form-horizontal" >
	    
	<table class="table table-bordered table-responsive">
	
    <tr>
    	<td><label class="control-label">1. Name (in block letter) 
(Specify how your name should appear in Directory)
</label></td>
        <td><input class="form-control" type="text" name="user_name" placeholder="Enter Username" value="<?php echo $username; ?>" /></td>
    </tr>
    
    <tr>
    	<td><label class="control-label">2.Date of Birth</label></td>
        <td><input class="form-control" type="text" name="dob" placeholder="Date of Birth" value="<?php echo $dob; ?>" /></td>
    </tr>
          <tr>
    	<td><label class="control-label">3.Age</label></td>
        <td><input class="form-control" type="text" name="age" placeholder="Age" value="<?php echo $age; ?>" /></td>
    </tr>    <tr>
    	<td><label class="control-label">4.Sex</label></td>
        <td><input class="form-control" type="text" name="sex" placeholder="Sex" value="<?php echo $sex; ?>" /></td>
    </tr>    <tr>
    	<td><label class="control-label">5.Present Position & Designation</label></td>
        <td><input class="form-control" type="text" name="ppd" placeholder="Present Position & Designation" value="<?php echo $ppd; ?>" /></td>
    </tr>    <tr>
    	<td><label class="control-label">6.Mobile Number</label></td>
        <td><input class="form-control" type="text" name="mno" placeholder="Mobile Number" value="<?php echo $mno; ?>" /></td>
    </tr>    <tr>
    	<td><label class="control-label">7.Residensial Number</label></td>
        <td><input class="form-control" type="text" name="rno" placeholder="Residensial Number" value="<?php echo $rno; ?>" /></td>
    </tr>    <tr>
    	<td><label class="control-label">8.Permanent Address</label></td>
        <td><input class="form-control" type="text" name="padd" placeholder="Enter Permanent Address Line 1" value="<?php echo $padd; ?>" /><BR>
		<input class="form-control" type="text" name="padd1" placeholder="Enter Permanent Address Line 2" value="<?php echo $padd; ?>" /><br>
		<input class="form-control" type="text" name="padd2" placeholder="Enter Permanent Address Line 3" value="<?php echo $padd; ?>" /></td>
    </tr>    <tr>
    	<td><label class="control-label">9.Present Address</label></td>
        <td><input class="form-control" type="text" name="ppadd" placeholder="Present Address" value="<?php echo $ppadd; ?>" />	<br>
		<input class="form-control" type="text" name="ppadd1" placeholder="Enter Present Address Line 2" value="<?php echo $padd; ?>" /><br>
		<input class="form-control" type="text" name="ppadd2" placeholder="Enter Present Address Line 3" value="<?php echo $padd; ?>" /></td>
    </tr> 
	<style>
.noborders td {
        border:0;
    }
	</style>


	<tr  >
    	<td colspan="2"><label class="control-label">10.Qualification </label>
		<table class="table table-bordered table-responsive">
                <tr id="heading">
                    <td>Degree</td>
                    <td>Year Of Passing</td>
                    <td>College</td>
                    <td>University</td>
                </tr>
                <tr>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
					
					
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                </tr>
				   <tr>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
					
					
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                </tr>
				   <tr>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
					
					
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                    <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
                </tr>

            </table> 
		</td>
		
       
		
		
    </tr>    <tr>
    	<td ><label class="control-label">11.Membership in other Socities</label></td>
        <td><input class="form-control" type="text" name="mem" placeholder="Your Profession" value="<?php echo $mem; ?>" /></td>
    </tr>    
   
                      <tr>
    	<td><label class="control-label">12.Profile Img.</label></td>
        <td><input class="input-group" type="file" name="user_image" accept="image/*" /></td>
    </tr>
    <tr>
        <td colspan="2"><button type="submit" name="btnsave" class="btn btn-default">
        <span class="glyphicon glyphicon-save"></span> &nbsp; Register
        </button>
        </td>
    </tr>
    
    </table>
    
</form>





    

</div>
					</div>
				</div>

				
			</div>
		</div>
		<!-- //Services -->


	<?php include('footer.php')?>
		<!--start-smooth-scrolling-->
						<script type="text/javascript">
									$(document).ready(function() {
										/*
										var defaults = {
								  			containerID: 'toTop', // fading element id
											containerHoverID: 'toTopHover', // fading element hover id
											scrollSpeed: 1200,
											easingType: 'linear' 
								 		};
										*/
										
										$().UItoTop({ easingType: 'easeOutQuart' });
										
									});
								</script>
								<!--end-smooth-scrolling-->
		<a href="#house" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	<script src="js/bootstrap.js"></script>

</body>
</html>